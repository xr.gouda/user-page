import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() => runApp(MaterialApp(
      home: UserPage(),
      debugShowCheckedModeBanner: false,
      theme:
          ThemeData(primaryIconTheme: IconThemeData(color: Colors.teal[700])),
    ));

class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  @override
  Widget build(BuildContext context) {
    bool isLargeScreen = false;

    if (MediaQuery.of(context).size.width > 530) {
      isLargeScreen = true;
    } else {
      isLargeScreen = false;
    }

    return isLargeScreen ? _buildTabletLayout() : _buildMobileLayout();
  }

  Widget _buildMobileLayout() {
    return Scaffold(
        endDrawer: Container(
          width: 160.0,
          child: Drawer(),
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              pinned: false,
              floating: false,
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              expandedHeight: 100.0,
              actions: <Widget>[],
              flexibleSpace: Wrap(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 40.0, top: 24.0),
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          //Dates Column
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 7),

                              //Day
                              Container(
                                alignment: Alignment.center,
                                height: 24.0,
                                width: 60.0,
                                decoration:
                                    BoxDecoration(color: Colors.teal[700]),
                                child: Text(
                                  "الأربعاء",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              //Date 1
                              Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: Text("١٤٤٠/۰٦/۱٦",
                                    style: TextStyle(fontSize: 12.0)),
                              ),
                              //Date 2
                              Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: Text("۲۰۱۹/۰۲/۲۰",
                                    style: TextStyle(fontSize: 12.0)),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: Text(
                                  "يوم عمل",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.teal[700]),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                              width: (MediaQuery.of(context).size.width / 2) -
                                  161.8),

                          //Line
                          Container(
                            width: 0.5,
                            height: 100,
                            decoration: BoxDecoration(color: Colors.black38),
                          ),

                          SizedBox(width: 8),

                          //Home Page Column (Home Icon With Word)
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(FontAwesomeIcons.home,
                                  color: Colors.teal[700], size: 35.0),
                              Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: Text("الرئيسية",
                                    style: TextStyle(
                                        fontSize: 13.0,
                                        fontWeight: FontWeight.w600)),
                              )
                            ],
                          ),
                          SizedBox(width: 12.0),

                          //Line
                          Container(
                            width: 0.5,
                            height: 100,
                            decoration: BoxDecoration(color: Colors.black38),
                          ),
                          SizedBox(width: 43.0),

                          //Right Column
                          new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  SizedBox(width: 3.0),
                                  Text("إختياريه",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16.0)),
                                  Text(" :مقرأة",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w600)),
                                ],
                              ),
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  SizedBox(width: 15),
                                  Text("تجريبية",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 15.0)),
                                  Text(" :حلقة",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w600)),
                                ],
                              ),
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text("شرق جدة",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 15.0)),
                                  Text(" :إشراف",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w600)),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),

            SliverList(
                delegate: SliverChildListDelegate([
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  CircleAvatar(
                    maxRadius: 23,
                    child: Text(
                      "مراجعة صغرى",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 11),
                    ),
                    backgroundColor: Colors.teal[700],
                  ),

                  SizedBox(width: 3),

                  CircleAvatar(
                    maxRadius: 23,
                    child: Icon(
                      FontAwesomeIcons.book,
                      size: 23,
                      color: Colors.white,
                    ),
                    backgroundColor: Colors.teal[700],
                  ),

                  SizedBox(width: 15),
                  //User Details
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Text(
                          "عبدالله عدنان جمل الليل",
                          style: TextStyle(
                            color: Colors.teal[800],
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 1),
                            child: Text("09/2",
                                style: TextStyle(
                                    color: Colors.teal[700],
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold)),
                          ),
                          SizedBox(width: 5),
                          Text(
                            "الاختبار",
                            style: TextStyle(fontSize: 14),
                          ),

                          SizedBox(width: 20),

                          //عدد الأجزاء
                          Padding(
                            padding: const EdgeInsets.only(bottom: 1),
                            child: Text("16",
                                style: TextStyle(
                                  color: Colors.teal[700],
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          SizedBox(width: 5),
                          Text(
                            "الأجزاء",
                            style: TextStyle(fontSize: 14),
                          ),
                        ],
                      ),

                      SizedBox(height: 4.0),

                      //Calendar Section
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("التقويم"),
                          SizedBox(width: 3.0),
                          Icon(
                            FontAwesomeIcons.calendarAlt,
                            color: Colors.teal[700],
                          ),
                        ],
                      ),
                    ],
                  ),

                  //Person Icon
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: CircleAvatar(
                      maxRadius: 23,
                      child: Icon(
                        Icons.person,
                        color: Colors.white,
                        size: 30,
                      ),
                      backgroundColor: Colors.teal[700],
                    ),
                  ),
                ],
              )
            ])),


            SliverList(
              delegate: SliverChildListDelegate([
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Container(
                      height: MediaQuery.of(context).size.height - 110,
                      width: 280,
                      child: new ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 6,
                        itemBuilder: (_, index) {
                          return Padding(
                            padding: const EdgeInsets.all(3),
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 10),
                                Row(
                                  children: <Widget>[
                                    Icon(Icons.settings, size: 15, color: Colors.grey,),
                                    SizedBox(width: 4),
                                    Text("مراجعة كبرى",
                                      style: TextStyle(
                                          color: Colors.teal[700],
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  ],
                                ),

                                SizedBox(height: 12),


                                //Box
                                Stack(
                                  alignment: Alignment.topLeft,
                                  children: <Widget>[
                                    //Bookmark Icon
                                    Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.bookmark,
                                          size: 16,
                                          color: Colors.orangeAccent,
                                        ),
                                        Text(
                                          "4",
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 7),
                                        )
                                      ],
                                    ),

                                    Positioned(
                                      right: 51,
                                      child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(Icons.more_vert),
                                        iconSize: 16,
                                        color: Colors.black38,
                                      ),
                                    ),

                                    Container(
                                      padding: EdgeInsets.only(top: 5),
                                      height: 80,
                                      width: 87,
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 15.0, bottom: 1.0),
                                                child: Text(
                                                  "30",
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontSize: 12),
                                                ),
                                              ),
                                              SizedBox(width: 5),
                                              Text(
                                                "المجادلة",
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            height: 1,
                                            width: 60,
                                            color: Colors.black38,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 16.0, bottom: 1.0),
                                                child: Text(
                                                  "30",
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontSize: 12),
                                                ),
                                              ),
                                              Text(
                                                "الحشر",
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                right: 3, left: 9),
                                            height: 18.0,
                                            width:
                                            MediaQuery.of(context).size.width,
                                            color: Colors.grey[200],
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  "12/2",
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.black38),
                                                ),
                                                Container(
                                                  width: 0.5,
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height,
                                                  color: Colors.black38,
                                                ),
                                                Text(
                                                  "-2.5",
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.black38),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),


                                Stack(
                                  alignment: Alignment.topLeft,
                                  children: <Widget>[
                                    //Bookmark Icon
                                    Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.bookmark,
                                          size: 16,
                                          color: Colors.orangeAccent,
                                        ),
                                        Text(
                                          "4",
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 7),
                                        )
                                      ],
                                    ),

                                    Positioned(
                                      right: 51,
                                      child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(Icons.more_vert),
                                        iconSize: 16,
                                        color: Colors.black38,
                                      ),
                                    ),

                                    Container(
                                      padding: EdgeInsets.only(top: 3),
                                      height: 80,
                                      width: 87,
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 15.0, bottom: 1.0),
                                                child: Text(
                                                  "30",
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontSize: 12),
                                                ),
                                              ),
                                              SizedBox(width: 5),
                                              Text(
                                                "المجادلة",
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            height: 1,
                                            width: 60,
                                            color: Colors.black38,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 16.0, bottom: 1.0),
                                                child: Text(
                                                  "30",
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontSize: 12),
                                                ),
                                              ),
                                              Text(
                                                "الحشر",
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                right: 3, left: 9),
                                            height: 18.0,
                                            width:
                                            MediaQuery.of(context).size.width,
                                            color: Colors.grey[200],
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  "12/2",
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.black38),
                                                ),
                                                Container(
                                                  width: 0.5,
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height,
                                                  color: Colors.black38,
                                                ),
                                                Text(
                                                  "-2.5",
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.black38),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),


                                Stack(
                                  alignment: Alignment.topLeft,
                                  children: <Widget>[
                                    //Bookmark Icon
                                    Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.bookmark,
                                          size: 16,
                                          color: Colors.orangeAccent,
                                        ),
                                        Text(
                                          "4",
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 7),
                                        )
                                      ],
                                    ),

                                    Positioned(
                                      right: 51,
                                      child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(Icons.more_vert),
                                        iconSize: 16,
                                        color: Colors.black38,
                                      ),
                                    ),

                                    Container(
                                      padding: EdgeInsets.only(top: 3),
                                      height: 80,
                                      width: 87,
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 15.0, bottom: 1.0),
                                                child: Text(
                                                  "30",
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontSize: 12),
                                                ),
                                              ),
                                              SizedBox(width: 5),
                                              Text(
                                                "المجادلة",
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            height: 1,
                                            width: 60,
                                            color: Colors.black38,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 16.0, bottom: 1.0),
                                                child: Text(
                                                  "30",
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontSize: 12),
                                                ),
                                              ),
                                              Text(
                                                "الحشر",
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                right: 3, left: 9),
                                            height: 18.0,
                                            width:
                                            MediaQuery.of(context).size.width,
                                            color: Colors.grey[200],
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  "12/2",
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.black38),
                                                ),
                                                Container(
                                                  width: 0.5,
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height,
                                                  color: Colors.black38,
                                                ),
                                                Text(
                                                  "-2.5",
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.black38),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),


                                Stack(
                                  alignment: Alignment.topLeft,
                                  children: <Widget>[
                                    //Bookmark Icon
                                    Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.bookmark,
                                          size: 16,
                                          color: Colors.orangeAccent,
                                        ),
                                        Text(
                                          "4",
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 7),
                                        )
                                      ],
                                    ),

                                    Positioned(
                                      right: 51,
                                      child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(Icons.more_vert),
                                        iconSize: 16,
                                        color: Colors.black38,
                                      ),
                                    ),

                                    Container(
                                      padding: EdgeInsets.only(top: 3),
                                      height: 80,
                                      width: 87,
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 15.0, bottom: 1.0),
                                                child: Text(
                                                  "30",
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontSize: 12),
                                                ),
                                              ),
                                              SizedBox(width: 5),
                                              Text(
                                                "المجادلة",
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            height: 1,
                                            width: 60,
                                            color: Colors.black38,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 16.0, bottom: 1.0),
                                                child: Text(
                                                  "30",
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontSize: 12),
                                                ),
                                              ),
                                              Text(
                                                "الحشر",
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                right: 3, left: 9),
                                            height: 18.0,
                                            width:
                                            MediaQuery.of(context).size.width,
                                            color: Colors.grey[200],
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  "12/2",
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.black38),
                                                ),
                                                Container(
                                                  width: 0.5,
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height,
                                                  color: Colors.black38,
                                                ),
                                                Text(
                                                  "-2.5",
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.black38),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),


                                Stack(
                                  alignment: Alignment.topLeft,
                                  children: <Widget>[
                                    //Bookmark Icon
                                    Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.bookmark,
                                          size: 16,
                                          color: Colors.orangeAccent,
                                        ),
                                        Text(
                                          "4",
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 7),
                                        )
                                      ],
                                    ),

                                    Positioned(
                                      right: 51,
                                      child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(Icons.more_vert),
                                        iconSize: 16,
                                        color: Colors.black38,
                                      ),
                                    ),

                                    Container(
                                      padding: EdgeInsets.only(top: 3),
                                      height: 80,
                                      width: 87,

                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 15.0, bottom: 1.0),
                                                child: Text(
                                                  "30",
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontSize: 12),
                                                ),
                                              ),
                                              SizedBox(width: 5),
                                              Text(
                                                "المجادلة",
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            height: 1,
                                            width: 60,
                                            color: Colors.black38,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 16.0, bottom: 1.0),
                                                child: Text(
                                                  "30",
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontSize: 12),
                                                ),
                                              ),
                                              Text(
                                                "الحشر",
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                right: 3, left: 9),
                                            height: 18.0,
                                            width:
                                            MediaQuery.of(context).size.width,
                                            color: Colors.grey[200],
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  "12/2",
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.black38),
                                                ),
                                                Container(
                                                  width: 0.5,
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height,
                                                  color: Colors.black38,
                                                ),
                                                Text(
                                                  "-2.5",
                                                  style: TextStyle(
                                                      fontSize: 11,
                                                      color: Colors.black38),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),


                              ],
                            ),
                          );
                        },
                      ),
                    ),

                    //Right Date Section
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[

                        Container(
                         padding: EdgeInsets.only(left: 6),
                          child: Icon(Icons.keyboard_arrow_up)
                        ),


                        //Date Right Section
                        Container(
                          padding: EdgeInsets.only(left: 5),
                          height: MediaQuery.of(context).size.height / 2 + 25,
                          width: MediaQuery.of(context).size.width / 2 - 100,
                          child: ListView.builder(
                            itemCount: 1,
                            itemBuilder: (_, index) {
                              return Container(
                                child: Column(
                                  children: <Widget>[
                                    dateDetails(
                                        day: "الأحد",
                                        number: "16",
                                        month: "جماد ا",
                                        isToday: true),
                                dateDetails(
                                    day: "الاثنين",
                                    number: "17",
                                    month: "جماد ا",
                                    isToday: false),
                                dateDetails(
                                    day: "الثلاثاء",
                                    number: "18",
                                    month: "جماد ا",
                                    isToday: false),
                                dateDetails(
                                    day: "الأربعاء",
                                    number: "19",
                                    month: "جماد ا",
                                    isToday: false),
                                dateDetails(
                                    day: "الخميس",
                                    number: "20",
                                    month: "جماد ا",
                                    isToday: false),

                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ]),
            ),
          ],
        ));
  }

  Widget _buildTabletLayout() {
    return Scaffold(
        endDrawer: Container(
          width: 160.0,
          child: Drawer(),
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              pinned: false,
              floating: false,
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              expandedHeight: 110.0,
              actions: <Widget>[],
              flexibleSpace: Wrap(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 40.0, top: 24.0),
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          //Dates Column
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 7),

                              //Day
                              Container(
                                alignment: Alignment.center,
                                height: 24.0,
                                width: 60.0,
                                decoration:
                                BoxDecoration(color: Colors.teal[700]),
                                child: Text(
                                  "الأربعاء",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              //Date 1
                              Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: Text("١٤٤٠/۰٦/۱٦",
                                    style: TextStyle(fontSize: 12.0)),
                              ),
                              //Date 2
                              Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: Text("۲۰۱۹/۰۲/۲۰",
                                    style: TextStyle(fontSize: 12.0)),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: Text(
                                  "يوم عمل",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.teal[700]),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                              width: (MediaQuery.of(context).size.width / 2) -
                                  161.8),

                          //Line
                          Container(
                            width: 0.5,
                            height: 100,
                            decoration: BoxDecoration(color: Colors.black38),
                          ),

                          SizedBox(width: 8),

                          //Home Page Column (Home Icon With Word)
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(FontAwesomeIcons.home,
                                  color: Colors.teal[700], size: 35.0),
                              Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: Text("الرئيسية",
                                    style: TextStyle(
                                        fontSize: 13.0,
                                        fontWeight: FontWeight.w600)),
                              )
                            ],
                          ),
                          SizedBox(width: 12.0),

                          //Line
                          Container(
                            width: 0.5,
                            height: 100,
                            decoration: BoxDecoration(color: Colors.black38),
                          ),
                          SizedBox(width: 43.0),

                          //Right Column
                          new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  SizedBox(width: 3.0),
                                  Text("إختياريه",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16.0)),
                                  Text(" :مقرأة",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w600)),
                                ],
                              ),
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  SizedBox(width: 15),
                                  Text("تجريبية",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 15.0)),
                                  Text(" :حلقة",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w600)),
                                ],
                              ),
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text("شرق جدة",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 15.0)),
                                  Text(" :إشراف",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w600)),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),

            SliverList(
                delegate: SliverChildListDelegate([
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      CircleAvatar(
                        maxRadius: 23,
                        child: Text(
                          "مراجعة صغرى",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 11),
                        ),
                        backgroundColor: Colors.teal[700],
                      ),

                      SizedBox(width: 3),

                      CircleAvatar(
                        maxRadius: 23,
                        child: Icon(
                          FontAwesomeIcons.book,
                          size: 23,
                          color: Colors.white,
                        ),
                        backgroundColor: Colors.teal[700],
                      ),

                      SizedBox(width: 15),
                      //User Details
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Text(
                              "عبدالله عدنان جمل الليل",
                              style: TextStyle(
                                color: Colors.teal[800],
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(bottom: 1),
                                child: Text("09/2",
                                    style: TextStyle(
                                        color: Colors.teal[700],
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold)),
                              ),
                              SizedBox(width: 5),
                              Text(
                                "الاختبار",
                                style: TextStyle(fontSize: 14),
                              ),

                              SizedBox(width: 20),

                              //عدد الأجزاء
                              Padding(
                                padding: const EdgeInsets.only(bottom: 1),
                                child: Text("16",
                                    style: TextStyle(
                                      color: Colors.teal[700],
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                    )),
                              ),
                              SizedBox(width: 5),
                              Text(
                                "الأجزاء",
                                style: TextStyle(fontSize: 14),
                              ),
                            ],
                          ),

                          SizedBox(height: 4.0),

                          //Calendar Section
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("التقويم"),
                              SizedBox(width: 3.0),
                              Icon(
                                FontAwesomeIcons.calendarAlt,
                                color: Colors.teal[700],
                              ),
                            ],
                          ),
                        ],
                      ),

                      //Person Icon
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: CircleAvatar(
                          maxRadius: 23,
                          child: Icon(
                            Icons.person,
                            color: Colors.white,
                            size: 30,
                          ),
                          backgroundColor: Colors.teal[700],
                        ),
                      ),
                    ],
                  )
                ])),


            SliverList(
              delegate: SliverChildListDelegate([
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Container(
                      height: MediaQuery.of(context).size.height - 110,
                      width: MediaQuery.of(context).size.width - 73,
                      child: new ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 6,
                        itemBuilder: (_, index) {
                          return Column(
                            children: <Widget>[
                              SizedBox(height: 10),
                              Row(
                                children: <Widget>[
                                  Icon(Icons.settings, size: 15, color: Colors.grey,),
                                  SizedBox(width: 4),
                                  Text("مراجعة كبرى",
                                    style: TextStyle(
                                        color: Colors.teal[700],
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ],
                              ),

                              SizedBox(height: 7),


                              //Box
                              Stack(
                                alignment: Alignment.topLeft,
                                children: <Widget>[
                                  //Bookmark Icon
                                  Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.bookmark,
                                        size: 16,
                                        color: Colors.orangeAccent,
                                      ),
                                      Text(
                                        "4",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 7),
                                      )
                                    ],
                                  ),

                                  Positioned(
                                    right: 51,
                                    child: IconButton(
                                      onPressed: () {},
                                      icon: Icon(Icons.more_vert),
                                      iconSize: 16,
                                      color: Colors.black38,
                                    ),
                                  ),

                                  Container(
                                    padding: EdgeInsets.only(top: 3),
                                    height: 80, //76
                                    width: 87, //83
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 15.0, bottom: 1.0),
                                              child: Text(
                                                "30",
                                                style: TextStyle(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                              ),
                                            ),
                                            SizedBox(width: 5),
                                            Text(
                                              "المجادلة",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 10),
                                          height: 1,
                                          width: 60,
                                          color: Colors.black38,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 16.0, bottom: 1.0),
                                              child: Text(
                                                "30",
                                                style: TextStyle(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                              ),
                                            ),
                                            Text(
                                              "الحشر",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: 3, left: 9),
                                          height: 18.0,
                                          width:
                                          MediaQuery.of(context).size.width,
                                          color: Colors.grey[200],
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                "12/2",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.black38),
                                              ),
                                              Container(
                                                width: 0.5,
                                                height: MediaQuery.of(context)
                                                    .size
                                                    .height,
                                                color: Colors.black38,
                                              ),
                                              Text(
                                                "-2.5",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.black38),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),


                              Stack(
                                alignment: Alignment.topLeft,
                                children: <Widget>[
                                  //Bookmark Icon
                                  Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.bookmark,
                                        size: 16,
                                        color: Colors.orangeAccent,
                                      ),
                                      Text(
                                        "4",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 7),
                                      )
                                    ],
                                  ),

                                  Positioned(
                                    right: 51,
                                    child: IconButton(
                                      onPressed: () {},
                                      icon: Icon(Icons.more_vert),
                                      iconSize: 16,
                                      color: Colors.black38,
                                    ),
                                  ),

                                  Container(
                                    padding: EdgeInsets.only(top: 3),
                                    height: 80, //76
                                    width: 87, //83
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 15.0, bottom: 1.0),
                                              child: Text(
                                                "30",
                                                style: TextStyle(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                              ),
                                            ),
                                            SizedBox(width: 5),
                                            Text(
                                              "المجادلة",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 10),
                                          height: 1,
                                          width: 60,
                                          color: Colors.black38,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 16.0, bottom: 1.0),
                                              child: Text(
                                                "30",
                                                style: TextStyle(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                              ),
                                            ),
                                            Text(
                                              "الحشر",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: 3, left: 9),
                                          height: 18.0,
                                          width:
                                          MediaQuery.of(context).size.width,
                                          color: Colors.grey[200],
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                "12/2",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.black38),
                                              ),
                                              Container(
                                                width: 0.5,
                                                height: MediaQuery.of(context)
                                                    .size
                                                    .height,
                                                color: Colors.black38,
                                              ),
                                              Text(
                                                "-2.5",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.black38),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),


                              Stack(
                                alignment: Alignment.topLeft,
                                children: <Widget>[
                                  //Bookmark Icon
                                  Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.bookmark,
                                        size: 16,
                                        color: Colors.orangeAccent,
                                      ),
                                      Text(
                                        "4",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 7),
                                      )
                                    ],
                                  ),

                                  Positioned(
                                    right: 51,
                                    child: IconButton(
                                      onPressed: () {},
                                      icon: Icon(Icons.more_vert),
                                      iconSize: 16,
                                      color: Colors.black38,
                                    ),
                                  ),

                                  Container(
                                    padding: EdgeInsets.only(top: 3),
                                    height: 80, //76
                                    width: 87, //83
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 15.0, bottom: 1.0),
                                              child: Text(
                                                "30",
                                                style: TextStyle(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                              ),
                                            ),
                                            SizedBox(width: 5),
                                            Text(
                                              "المجادلة",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 10),
                                          height: 1,
                                          width: 60,
                                          color: Colors.black38,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 16.0, bottom: 1.0),
                                              child: Text(
                                                "30",
                                                style: TextStyle(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                              ),
                                            ),
                                            Text(
                                              "الحشر",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: 3, left: 9),
                                          height: 18.0,
                                          width:
                                          MediaQuery.of(context).size.width,
                                          color: Colors.grey[200],
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                "12/2",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.black38),
                                              ),
                                              Container(
                                                width: 0.5,
                                                height: MediaQuery.of(context)
                                                    .size
                                                    .height,
                                                color: Colors.black38,
                                              ),
                                              Text(
                                                "-2.5",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.black38),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),


                              Stack(
                                alignment: Alignment.topLeft,
                                children: <Widget>[
                                  //Bookmark Icon
                                  Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.bookmark,
                                        size: 16,
                                        color: Colors.orangeAccent,
                                      ),
                                      Text(
                                        "4",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 7),
                                      )
                                    ],
                                  ),

                                  Positioned(
                                    right: 51,
                                    child: IconButton(
                                      onPressed: () {},
                                      icon: Icon(Icons.more_vert),
                                      iconSize: 16,
                                      color: Colors.black38,
                                    ),
                                  ),

                                  Container(
                                    padding: EdgeInsets.only(top: 3),
                                    height: 80, //76
                                    width: 87, //83
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 15.0, bottom: 1.0),
                                              child: Text(
                                                "30",
                                                style: TextStyle(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                              ),
                                            ),
                                            SizedBox(width: 5),
                                            Text(
                                              "المجادلة",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 10),
                                          height: 1,
                                          width: 60,
                                          color: Colors.black38,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 16.0, bottom: 1.0),
                                              child: Text(
                                                "30",
                                                style: TextStyle(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                              ),
                                            ),
                                            Text(
                                              "الحشر",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: 3, left: 9),
                                          height: 18.0,
                                          width:
                                          MediaQuery.of(context).size.width,
                                          color: Colors.grey[200],
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                "12/2",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.black38),
                                              ),
                                              Container(
                                                width: 0.5,
                                                height: MediaQuery.of(context)
                                                    .size
                                                    .height,
                                                color: Colors.black38,
                                              ),
                                              Text(
                                                "-2.5",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.black38),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),


                              Stack(
                                alignment: Alignment.topLeft,
                                children: <Widget>[
                                  //Bookmark Icon
                                  Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.bookmark,
                                        size: 16,
                                        color: Colors.orangeAccent,
                                      ),
                                      Text(
                                        "4",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 7),
                                      )
                                    ],
                                  ),

                                  Positioned(
                                    right: 51,
                                    child: IconButton(
                                      onPressed: () {},
                                      icon: Icon(Icons.more_vert),
                                      iconSize: 16,
                                      color: Colors.black38,
                                    ),
                                  ),

                                  Container(
                                    padding: EdgeInsets.only(top: 3),
                                    height: 80, //76
                                    width: 87, //83
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 15.0, bottom: 1.0),
                                              child: Text(
                                                "30",
                                                style: TextStyle(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                              ),
                                            ),
                                            SizedBox(width: 5),
                                            Text(
                                              "المجادلة",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 10),
                                          height: 1,
                                          width: 60,
                                          color: Colors.black38,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 16.0, bottom: 1.0),
                                              child: Text(
                                                "30",
                                                style: TextStyle(
                                                    color: Colors.grey[600],
                                                    fontSize: 12),
                                              ),
                                            ),
                                            Text(
                                              "الحشر",
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: 3, left: 9),
                                          height: 18.0,
                                          width:
                                          MediaQuery.of(context).size.width,
                                          color: Colors.grey[200],
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                "12/2",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.black38),
                                              ),
                                              Container(
                                                width: 0.5,
                                                height: MediaQuery.of(context)
                                                    .size
                                                    .height,
                                                color: Colors.black38,
                                              ),
                                              Text(
                                                "-2.5",
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.black38),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),


                            ],
                          );
                        },
                      ),
                    ),

                    //Right Date Section
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[

                        Container(
                            child: Icon(Icons.keyboard_arrow_up)
                        ),

                        Container(
                          height: MediaQuery.of(context).size.height / 2 + 20,//320
                          width: MediaQuery.of(context).size.width / 2 - 200,
                          child: ListView.builder(
                            itemCount: 2,
                            itemBuilder: (_, index) {
                              return Container(
                                child: Column(
                                  children: <Widget>[
                                    dateDetailsTablet(
                                        day: "الأحد",
                                        number: "16",
                                        month: "جماد ا",
                                        isToday: true),
                                    dateDetailsTablet(
                                        day: "الاثنين",
                                        number: "17",
                                        month: "جماد ا",
                                        isToday: false),
                                    dateDetailsTablet(
                                        day: "الثلاثاء",
                                        number: "18",
                                        month: "جماد ا",
                                        isToday: false),
                                    dateDetailsTablet(
                                        day: "الأربعاء",
                                        number: "19",
                                        month: "جماد ا",
                                        isToday: false),
                                    dateDetailsTablet(
                                        day: "الخميس",
                                        number: "20",
                                        month: "جماد ا",
                                        isToday: false),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ]),
            ),
          ],
        ));
  }

  Widget dateDetails(
      {@required String day,
      @required String number,
      @required String month,
      @required bool isToday}) {
    return Container(
      height: 75,
      width: MediaQuery.of(context).size.width / 2 - 95,
      decoration: BoxDecoration(
          color: isToday == false ? Colors.white : Colors.teal[700]),
      child: Wrap(
        alignment: WrapAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 3.0),
                child: Text(
                  day,
                  style: TextStyle(
                      color: isToday == false ? Colors.grey[600] : Colors.white,
                      fontSize: 15),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 3.0),
                child: Text(
                  number,
                  style: TextStyle(
                      color: isToday == false ? Colors.teal[700] : Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Text(
                      month,
                      style: TextStyle(
                        color:
                            isToday == false ? Colors.grey[600] : Colors.white,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget dateDetailsTablet(
      {@required String day,
      @required String number,
      @required String month,
      @required bool isToday}) {
    return Container(
      height: 90,
      width: MediaQuery.of(context).size.width / 2 - 100,
      decoration: BoxDecoration(
          color: isToday == false ? Colors.white : Colors.teal[700]),
      child: Wrap(
        alignment: WrapAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 3.0),
                child: Text(
                  day,
                  style: TextStyle(
                      color: isToday == false ? Colors.grey[600] : Colors.white,
                      fontSize: 15),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 3.0),
                child: Text(
                  number,
                  style: TextStyle(
                      color: isToday == false ? Colors.teal[700] : Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Text(
                      month,
                      style: TextStyle(
                        color:
                        isToday == false ? Colors.grey[600] : Colors.white,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
